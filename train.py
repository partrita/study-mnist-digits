import copy
from cottonwood.activation import Logistic, TanH
from cottonwood.conv2d import Conv2D
from cottonwood.data.mnist import TrainingData
from cottonwood.linear import Linear
from cottonwood.loggers import ValueLogger
from cottonwood.loss import Hinge
from cottonwood.normalization import Bias
from cottonwood.operations import Copy, Flatten, HardMax, OneHot
from cottonwood.optimization import SGD
from cottonwood.pooling import MaxPool2D
from cottonwood.structure import save_structure, Structure

learning_rate = 1e-3
kernel_size = 3
l1_param = 1e-4
l2_param = 1e-5
n_classes = 10
n_kernels = 16
n_training_iterations = 1e6
save_interval = 2e4
model_filename = "mnist_classifier.pkl"

training_data = TrainingData()
optimizer = SGD(learning_rate=learning_rate)

classifier = Structure()
classifier.add(training_data, "training_data")
classifier.add(Conv2D(
    kernel_size=kernel_size,
    l1_param=l1_param,
    l2_param=l2_param,
    n_kernels=n_kernels,
    optimizer=optimizer,
), "conv_2D_0")
classifier.add(Bias(), "bias_0")
classifier.add(TanH(), "act_0")
classifier.add(Conv2D(
    kernel_size=kernel_size,
    l1_param=l1_param,
    l2_param=l2_param,
    n_kernels=n_kernels,
    optimizer=optimizer,
), "conv_2D_1")
classifier.add(Bias(), "bias_1")
classifier.add(TanH(), "act_1")
classifier.add(MaxPool2D(stride=2, window=3), "max_pool_2D")
classifier.add(Flatten(), "flatten")
classifier.add(Linear(
    l1_param=l1_param,
    l2_param=l2_param,
    n_nodes=n_classes,
    optimizer=optimizer,
), "linear_2")
classifier.add(Bias(), "bias_2")
classifier.add(Logistic(), "logistic_2")
classifier.add(Copy(), "prediction")
classifier.add(Hinge(), "loss")
classifier.add(OneHot(n_categories=n_classes), "one_hot")
classifier.add(HardMax(), "hard_max")

classifier.connect_sequence([
    "training_data",
    "conv_2D_0",
    "bias_0",
    "act_0",
    "conv_2D_1",
    "bias_1",
    "act_1",
    "max_pool_2D",
    "flatten",
    "linear_2",
    "bias_2",
    "logistic_2",
    "prediction",
    "loss",
])
classifier.connect("training_data", "one_hot", i_port_tail=1)
classifier.connect("one_hot", "loss", i_port_head=1)
classifier.connect("prediction", "hard_max", i_port_tail=1)

loss_logger = ValueLogger(
    n_iter_report=1e3,
    reporting_bin_size=1e3,
    value_name="loss",
)

for i_iter in range(int(n_training_iterations)):
    classifier.forward_pass()
    loss_logger.log_value(classifier.blocks["loss"].loss)
    classifier.backward_pass()
    if i_iter % save_interval == 0:
        model_for_saving = copy.deepcopy(classifier)
        model_for_saving.remove("training_data")
        save_structure(model_filename, model_for_saving)
